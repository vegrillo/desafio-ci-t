<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Exercicio 4</title>
  <style>.form-group.col-md-6{float: none;}</style>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery-3.3.1.min"></script>
</head>
<body>
<?php  

   $fileContents = json_decode(file_get_contents('db.txt'));

   //PEGA OS DADOS ENVIADOS PELO FORMULÁRIO 
   $nome = $_POST['nome'];

   $sobrenome = $_POST['sobrenome'];

   $email = $_POST['email'];
   if(!empty($fileContents)){
     if(in_array($email, $fileContents)){
        echo "<h4> Email já Cadastrado, Tente Novamente!";
        echo "<div class='form-group col-md-6'><a href='Ex4.php'><input type='button' value='Voltar' class='btn btn-primary'></a></div>"; 
        exit();
     }
    }

   $telefone = $_POST['telefone'];

   $login = $_POST['login'];
   if(!empty($fileContents)){
     if(in_array($login, $fileContents)){
        echo "<h4> Login já Cadastrado, Tente Novamente!";
        echo "<div class='form-group col-md-6'><a href='Ex4.php'><input type='button' value='Voltar' class='btn btn-primary'></a></div>"; 
        exit();
     }
    }

   $senha = base64_encode($_POST['senha']);

   //PREPARA O CONTEÚDO A SER GRAVADO 
   $content = array("$nome"."$sobrenome","$email","$telefone", "login", "$senha"); 
   if(!empty($fileContents)){
     $content = array_merge($fileContents, $content);   
   }
   $encodedContent = json_encode($content);

   //ARQUIVO TXT 
   $arquivo = "db.txt";

   //TENTA ABRIR O ARQUIVO TXT 
   if (!$abrir = fopen($arquivo, "w+")) { 
   echo "<h4> Erro abrindo arquivo ($arquivo) </h4>"; 
   exit(); 
   } 

   //ESCREVE NO ARQUIVO TXT 
   
   if (!fwrite($abrir, $encodedContent)) { 
   print "<h4> Erro escrevendo no arquivo ($arquivo) </h4>"; 
   exit(); 
   }else{
       echo "<h4>Arquivo gravado com Sucesso !!</h4>";
       echo "<div class='form-group col-md-6'><a href='Ex4.php'><input type='button' value='Voltar' class='btn btn-primary'></a></div>";   
   } 

?>
</body>
</html>