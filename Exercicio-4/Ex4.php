<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Exercicio 4</title>
  <style>.form-group.col-md-6{float: none;}</style>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery-3.3.1.min"></script>
</head>
<body>
    <div class="form-group col-md-8">
      <h2>Cadastro Desafio Ci&T</h2>
    </div>
     <div class="form-group col-md-8">  
       <form id="formExemplo" data-toggle="validator" role="form" method="POST" action="salvar.php">   
          <div class="form-group col-md-6">      
            <label for="textNome" class="control-label">Nome</label>      
            <input id="textNome" name="nome" class="form-control" placeholder="Digite seu Nome..." type="text" required>    
          </div>
          <div class="form-group col-md-6">      
            <label for="textSobrenome" class="control-label">Sobrenome</label>      
            <input id="textSobrenome" name="sobrenome" class="form-control" placeholder="Digite seu Sobrenome..." type="text" required> 
          </div>         
          <div class="form-group col-md-6">      
            <label for="inputEmail" class="control-label">Email</label>       
            <input id="inputEmail" name="email" class="form-control" placeholder="Digite seu E-mail" type="email" data-error="Por favor, informe um e-mail correto." required>      
            <div class="help-block with-errors"></div>    
          </div>
          <div class="form-group col-md-6">      
            <label for="inputTelefone" class="control-label">Telefone</label>     
            <input id="inputTelefone" name="telefone" class="form-control" placeholder="Digite seu Telefone" type="telefone" data-error="Por favor, informe um telefone no formato (xx) xxxx-xxxx." required>      
            <div class="help-block with-errors"></div>    
          </div>
          <div class="form-group col-md-6">      
            <label for="textLogin" class="control-label">Login</label>      
            <input id="textLogin" name="login" class="form-control" placeholder="Digite seu Login..." type="text" required>    
          </div>                      
          <div class="form-group col-md-6">      
            <label for="inputSenha" class="control-label">Senha</label>      
            <input type="senha" name="senha" class="form-control" id="inputSenha" placeholder="Digite sua Senha..."          data-minlength="6" required>      
            <span class="help-block">Mínimo de seis (6) digitos</span>    
          </div>            
          <div class="form-group col-md-6">      
            <button type="submit" class="btn btn-primary">Enviar</button>   
          </div>  

       </form>  
    </div>
<script src="js/validator.min.js"></script>

</body>
</html>