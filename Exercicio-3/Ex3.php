<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Exercicio 3</title>
</head>
<body>
  <?php

  $files = array("music.mp4", "video.mov", "imagem.jpeg");

  echo "<ol type='a'>";
  foreach ($files as $key => $value) {
    echo "<li> ".$value."</li>"; 
  }
  echo "</ol>";

  $reversed = array_reverse($files);
  ksort($reversed);

  echo "<ol type='1'>";
  foreach ($reversed as $key => $value) {
    $arr_val = explode(".",$value);
    echo "<li> .".$arr_val[1]."</li>"; 
  }
  echo "</ol>";

  ?>
</body>
</html>