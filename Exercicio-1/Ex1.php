<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Exercicio 1</title>
</head>
<body>
  <?php

  $locations = array("Brasil" => "Brasilia", "EUA" => "Washington", "Haiti" => "Porto Príncipe", "Chile" => "Santiago", "Japão" => "Tóquio", "Afeganistão" => "Cabul");

  ksort($locations);

  foreach ($locations as $key => $value) {
    echo "<p> A Capital do <b> $key </b> é <b> $value </b> </p>";
  }

  ?>
</body>
</html>